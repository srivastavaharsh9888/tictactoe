from django.conf.urls import url,include
from .views import WelcomeView,SigninView,SignupView,homepage,saveWin,logout_view

urlpatterns=[
	url('^$',WelcomeView.as_view(),name='home'),
	url(r'^welcome/$',WelcomeView.as_view(),name='home'),
	url(r'^login/$', SigninView, name='login'),
    url(r'^signup/$', SignupView, name='signup'),
    url(r'^homepage/$', homepage, name='homepage'),
 	#url(r'^save/turn/$$', saveGameData, name='game-data'),   
 	url(r'^save/win/$$', saveWin, name='game-data'),   
 	 url(r'^logout/$$', logout_view, name='game-data'),   
]