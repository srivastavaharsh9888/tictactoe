from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from rest_framework import status
from .models import Game


class WelcomeView(TemplateView):
    template_name = 'core/welcome.html'

def SigninView(request):
	if request.method=="POST":
		user=authenticate(username=request.POST.get('email'),password=request.POST.get('password'))
		if user:
			login(request,user)
			return redirect('/homepage/')
		else:
			return render(request,'core/welcome.html',{"error":"Incorrect Password"})				
	return render(request,'core/welcome.html')

def logout_view(request):
	logout(request)
	return redirect("/")

def SignupView(request):
	if request.method=="POST":
		data=request.POST.copy()
		email=data.get('email')
		first_name=data.get('FirstName')
		last_name=data.get('LastName')
		password=data.get("password")
		try:
			user=User.objects.create(username=str(email),first_name=first_name,last_name=last_name)
			user.set_password(password)
			user.save()
		except Exception as e:
			return render(request,'core/welcome.html',{"error":"User with this email already exists"})
		return redirect("/welcome/")
	return  redirect('/welcome/')	

@login_required(login_url="/")
def homepage(request):
	game_won=Game.objects.filter(user=request.user,win=True)
	return render(request,'core/game.html',{"win":"You have won "+str(game_won.count())})

'''@csrf_exempt
def saveGameData(request):
	if request.method=="POST":
		print(request.POST)
		if request.POST["gameId"]!="undefined":
			print("herea except")
			return JsonResponse({"hello":"asdhjs"},status=status.HTTP_200_OK)
		else:
			sign=request.POST["sign"]
			try:
				print("herea try")
				k=Game.objects.get(id=int(request.POST["id"]))
			except:
				print("herea except")
				k=Game.objects.create(user=request.user,sign=request.POST["sign"])	
				return JsonResponse({"gameId":k.id},status=status.HTTP_200_OK)
		print(request.user)
		return JsonResponse({"ashdjk":"akjsdhsk"},status=status.HTTP_200_OK)
	else:
		return JsonResponse({"message":"not allowed"},status=status.HTTP_400_BAD_REQUEST)'''

@csrf_exempt
def saveWin(request):
	if request.method=="POST":
		try:
			print(request.POST)
			if request.POST["win"]=="user":
				k=Game.objects.create(user=request.user,win=True)
			else:	
				k=Game.objects.create(user=request.user)
			return redirect('/homepage/')
		except:
			return JsonResponse({"message":"not allowed"},status=status.HTTP_400_BAD_REQUEST)
	else:
		return JsonResponse({"message":"not allowed"},status=status.HTTP_400_BAD_REQUEST)

