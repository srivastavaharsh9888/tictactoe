from django.db import models
from django.contrib.auth.models import User

class Game(models.Model):
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	detail=models.CharField(max_length=30,default="1,2,3,4,5,6,7,8,9")
	turn=models.BooleanField(default=0)
	completed=models.BooleanField(default=0)
	sign=models.CharField(max_length=5)
	completed=models.BooleanField(default=0)
	win=models.BooleanField(default=0)
